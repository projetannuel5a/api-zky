package com.example.zky.cart.controller;

import com.example.zky.account.authentication.AuthenticatedRequestBody;
import com.example.zky.account.authentication.AuthenticationManager;
import com.example.zky.account.authentication.Token;
import com.example.zky.account.exceptions.MustBeAuthenticatedAsAdminException;
import com.example.zky.account.model.Account;
import com.example.zky.account.model.AccountPublic;
import com.example.zky.account.repository.ZkyInfoRepository;
import com.example.zky.account.service.AccountService;
import com.example.zky.cart.model.BodyCart;
import com.example.zky.cart.model.Cart;
import com.example.zky.cart.model.CartLine;
import com.example.zky.cart.model.Status;
import com.example.zky.cart.publicModel.CartLinePublic;
import com.example.zky.cart.publicModel.CartPublic;
import com.example.zky.cart.publicModel.CartPublicData;
import com.example.zky.cart.repository.CartLineRepository;
import com.example.zky.cart.repository.CartRepository;
import com.example.zky.mail.MailService;
import com.example.zky.product.model.Product;
import com.example.zky.product.publicModel.ProductPublic;
import com.example.zky.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Kevin on 01/07/2017 for ZKY.
 */
@RestController
@CrossOrigin
@RequestMapping("/cart")
public class CartController {
    private final AccountService accountService;
    private final ProductRepository productRepository;
    private final AuthenticationManager authenticationManager;
    private final CartRepository cartRepository;
    private final CartLineRepository cartLineRepository;
    private final MailService emailService;
    private final ZkyInfoRepository zkyInfoRepository;


    @Autowired
    public CartController(AccountService accountService, ProductRepository productRepository, AuthenticationManager authenticationManager, CartRepository cartRepository, CartLineRepository cartLineRepository, MailService emailService, ZkyInfoRepository zkyInfoRepository) {
        this.accountService = accountService;
        this.productRepository = productRepository;
        this.authenticationManager = authenticationManager;
        this.cartRepository = cartRepository;
        this.cartLineRepository = cartLineRepository;
        this.emailService = emailService;
        this.zkyInfoRepository = zkyInfoRepository;
    }

    /**
     * Get cart using id cart and admin token
     *
     * @param id    id cart
     * @param token admin
     * @return Cart public
     * @throws Exception not admin
     */
    @RequestMapping(value = "/{id}", method = POST)
    public CartPublic getCart(@PathVariable("id") int id, @RequestBody Token token) throws Exception {
        authenticationManager.mustBeValidToken(token.getToken());
        Account account = authenticationManager.getAccountFromToken(token.getToken());
        if (account.isAdmin()) {
            return new CartPublic(cartRepository.findCartById(id), account);
        } else {
            List<Cart> cartList = account.getOldCarts();
            Cart cart = cartRepository.findCartById(id);
            for (Cart c :
                    cartList) {
                if (c.getId() == cart.getId()) {
                    return new CartPublic(cart, account);
                }

            }
            throw new MustBeAuthenticatedAsAdminException();
        }

    }

    /**
     * Validate cart status user id and admin token.
     *
     * @param id    id cart
     * @param token token admin
     * @return boolean
     * @throws Exception not admin
     */
    @RequestMapping(value = "/validated/{id}", method = POST)
    public boolean setCartValidated(@PathVariable("id") int id, @RequestBody Token token) throws Exception {
        authenticationManager.mustBeValidToken(token.getToken());
        Account account = authenticationManager.getAccountFromToken(token.getToken());
        if (account.isAdmin()) {
            Cart cart = cartRepository.findCartById(id);
            cart.setStatus(Status.VALIDATED);
            cartRepository.save(cart);
            return true;
        } else {
            throw new MustBeAuthenticatedAsAdminException();
        }
    }

    /**
     * Get current cart using token user
     *
     * @param token token user
     * @return cart public
     */
    @RequestMapping(method = POST)
    public CartPublic getCurrentCart(@RequestBody Token token) {
        authenticationManager.mustBeValidToken(token.getToken());
        Account account = authenticationManager.getAccountFromToken(token.getToken());
        return new CartPublic(account.getCurrentCart(), account);
    }

    /**
     * Get list of old carts
     *
     * @param cart cart
     * @return return cart public
     */
    @RequestMapping(value = "/history", method = POST)
    public List<CartPublic> getOldCarts(@RequestBody AuthenticatedRequestBody<Cart> cart) {
        authenticationManager.mustBeValidToken(cart.getToken());
        Account account = authenticationManager.getAccountFromToken(cart.getToken());
        List<CartPublic> cartPublics = new ArrayList<>();
        for (Cart cl :
                account.getOldCarts()) {
            cartPublics.add(new CartPublic(cl, account));
        }
        return cartPublics;
    }

    /**
     * Set quantity and product in cart line
     *
     * @param cart cart
     * @return cart public
     */
    @RequestMapping(value = "/set", method = POST)
    public CartPublic setCartLine(@RequestBody AuthenticatedRequestBody<BodyCart> cart) {
        return updateCartLine(true, cart);
    }

    /**
     * Add quantity and product in cart line
     *
     * @param cart cart
     * @return cart public
     */
    @RequestMapping(value = "/add", method = POST)
    public CartPublic addCartLine(@RequestBody AuthenticatedRequestBody<BodyCart> cart) {
        return updateCartLine(false, cart);
    }

    /**
     * Validate user cart , send email recap of wish list to ZKY and client
     *
     * @param token token user
     * @return boolean
     */
    @RequestMapping(value = "/validate", method = POST)
    public boolean validateCart(@RequestBody Token token) {

        authenticationManager.mustBeValidToken(token.getToken());
        Account account = authenticationManager.getAccountFromToken(token.getToken());
        Cart currentCart = account.getCurrentCart();

        //check validate empty cart
        if (currentCart.getCartLines().isEmpty()) {
            return false;
        }
        //Update cart status and change cart
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        format.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        Cart cart = new Cart();
        List<Cart> oldCArts = account.getOldCarts();
        currentCart.setStatus(Status.PENDING);
        currentCart.setValidationDate(format.format(date));
        oldCArts.add(currentCart);


        //Send mail with current cart current cart
        float price = 0;
        for (CartLine cl : currentCart.getCartLines()) {
            price += cl.getQuantity() * Float.valueOf(cl.getProduct().getPrixdevente());
        }
        String name = account.getNom();
        if (name == null) {
            name = account.getLogin();
        } else if (name.isEmpty()) {
            name = account.getLogin();
        }
        String mailTo;
        try {
            mailTo = zkyInfoRepository.findAll().get(0).getMail();
        } catch (Exception e) {
            mailTo = "zky@live.fr";
        }
        if (mailTo == null) {
            mailTo = "zky@live.fr";
        }
        emailService.validateCart(account.getEmail(), name, currentCart.getCartLines(), String.valueOf(price), false);
        emailService.validateCart(mailTo, name, currentCart.getCartLines(), String.valueOf(price), true);


        cart.setStatus(Status.DOING);
        cartRepository.save(cart);
        account.setCurrentCart(cart);
        accountService.saveAccount(account);
        return true;
    }


    /**
     * Get list en pending cart
     *
     * @param token token admin
     * @return list cart public
     * @throws Exception not admin
     */
    @RequestMapping(value = "/pending", method = POST)
    public List<CartPublic> pendingCart(@RequestBody Token token) throws Exception {
        return findByStatus(token.getToken(), Status.PENDING);
    }

    /**
     * Get list en doing cart
     *
     * @param token token admin
     * @return list cart public
     * @throws Exception not admin
     */
    @RequestMapping(value = "/doing", method = POST)
    public List<CartPublic> doingCart(@RequestBody Token token) throws Exception {
        return findByStatus(token.getToken(), Status.DOING);
    }

    /**
     * Get list en validated cart
     *
     * @param token token admin
     * @return list cart public
     * @throws Exception not admin
     */
    @RequestMapping(value = "/validated", method = POST)
    public List<CartPublic> validatedCart(@RequestBody Token token) throws Exception {
        return findByStatus(token.getToken(), Status.VALIDATED);
    }

    /**
     * Update cartile with received info
     *
     * @param isSet boolean for set of add
     * @param cart  cart info
     * @return cart public
     */
    private CartPublic updateCartLine(Boolean isSet, AuthenticatedRequestBody<BodyCart> cart) {
        authenticationManager.mustBeValidToken(cart.getToken());
        Account account = authenticationManager.getAccountFromToken(cart.getToken());
        Cart cart1 = account.getCurrentCart();
        Product product = productRepository.findById(cart.getBody().getProductId());
        CartLine cartLine;
        int quantity;
        List<CartLine> cartLines = cart1.getCartLines();
        if (cartLines == null) {
            cartLines = new ArrayList<>();
            cartLine = new CartLine();
            cartLine.setProduct(product);
            cartLine.setQuantity(cart.getBody().getQuantity());
            cartLines.add(cartLine);
        } else {
            boolean tester = false;
            for (CartLine cartLine1 : cartLines) {
                if (cartLine1.getProduct().equals(product)) {
                    if (isSet) {
                        quantity = cart.getBody().getQuantity();
                    } else {
                        quantity = cartLine1.getQuantity() + cart.getBody().getQuantity();
                    }
                    if (quantity == 0) {
                        cartLines.remove(cartLine1);
                    } else {
                        cartLine1.setQuantity(quantity);
                    }
                    tester = true;
                    break;
                }
            }
            if (!tester) {
                cartLine = new CartLine();
                cartLine.setProduct(product);
                cartLine.setQuantity(cart.getBody().getQuantity());
                cartLines.add(cartLine);
            }
        }
        List<CartLinePublic> cartPublics = new ArrayList<>();
        for (CartLine c :
                cartLines) {
            cartPublics.add(new CartLinePublic(c.getQuantity(), new ProductPublic(c.getProduct())));
        }
        cart1.setCartLines(cartLines);
        cartLineRepository.save(cartLines);
        cartRepository.save(cart1);
        return new CartPublic(new CartPublicData(cart1.getId(), cart1.getStatus(), cart1.getValidationDate()), new AccountPublic(account), cartPublics);
    }


    /**
     * Get cart by status
     *
     * @param token  admin token
     * @param status Enum status
     * @return list cart public
     * @throws Exception not admin
     */
    private List<CartPublic> findByStatus(String token, Status status) throws Exception {
        authenticationManager.mustBeValidToken(token);
        Account account = authenticationManager.getAccountFromToken(token);
        if (account.isAdmin()) {
            List<Cart> cartList = cartRepository.findAllByStatusEquals(status);
            List<CartPublic> cartPublics = new ArrayList<>();
            for (Cart c :
                    cartList) {
                Account account1;
                if (status.equals(Status.DOING)) {
                    account1 = accountService.findByCurrentCart(c);
                } else if (status.equals(Status.PENDING)) {
                    account1 = accountService.findByOldCart(c);
                } else {

                    account1 = accountService.findByOldCart(c);
                }
                cartPublics.add(new CartPublic(c, account1));
            }
            return cartPublics;
        }

        throw new MustBeAuthenticatedAsAdminException();
    }
}
