package com.example.zky.cart.repository;

import com.example.zky.cart.model.CartLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Kevin on 01/07/2017 for ZKY.
 * Repository for cart line
 */
@Repository
public interface CartLineRepository extends JpaRepository<CartLine, Long> {
}
