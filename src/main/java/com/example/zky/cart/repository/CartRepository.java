package com.example.zky.cart.repository;

import com.example.zky.cart.model.Cart;
import com.example.zky.cart.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Kevin on 02/07/2017 for ZKY.
 * Repository for cart
 */
@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {
    List<Cart> findAllByStatusEquals(Status status);
    Cart findCartById(int id);
}
