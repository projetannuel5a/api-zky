package com.example.zky.cart.publicModel;

import com.example.zky.product.publicModel.ProductPublic;
import lombok.*;

/**
 * Created by Kevin on 02/07/2017 for ZKY.
 * Object public cart line
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartLinePublic {

    int quantity;
    ProductPublic productPublics;


}
