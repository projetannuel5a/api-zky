package com.example.zky.cart.publicModel;

import com.example.zky.account.model.Account;
import com.example.zky.account.model.AccountPublic;
import com.example.zky.cart.model.Cart;
import com.example.zky.cart.model.CartLine;
import com.example.zky.product.publicModel.ProductPublic;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin on 02/07/2017 for ZKY.
 * object public cart
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartPublic {
    CartPublicData cart;
    AccountPublic accountPublic;
    List<CartLinePublic> cartLinePublics;


    public CartPublic(Cart cart, Account account) {
        this.cart = new CartPublicData(cart.getId(), cart.getStatus(), cart.getValidationDate());
        List<CartLinePublic> cartLinePublics = new ArrayList<>();
        for (CartLine cl :
                cart.getCartLines()) {
            cartLinePublics.add(new CartLinePublic(cl.getQuantity(), new ProductPublic(cl.getProduct())));
        }
        this.cartLinePublics = cartLinePublics;
        this.accountPublic = new AccountPublic(account);
    }
}
