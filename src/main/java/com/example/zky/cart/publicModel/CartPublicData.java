package com.example.zky.cart.publicModel;

import com.example.zky.cart.model.Status;
import lombok.*;

/**
 * Created by Kevin on 06/07/2017 for ZKY.
 * object public cart data
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartPublicData {

    private int id;
    private Status status;
    private String validationDate;


}
