package com.example.zky.cart.model;

/**
 * Created by Andreï on 30/06/2017 for ZKY.
 * Object Cart in db
 */

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "CART")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "status")
    private Status status;

    @Column(name = "validationdate")
    private String validationDate;

    @OneToMany
    @JoinColumn(name = "cartlines")
    private List<CartLine> cartLines;

}