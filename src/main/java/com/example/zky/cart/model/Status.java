package com.example.zky.cart.model;

/**
 * Created by Kevin on 06/07/2017 for ZKY.
 * Enum status cart
 */
public enum Status {
    DOING,
    PENDING,
    VALIDATED,

}
