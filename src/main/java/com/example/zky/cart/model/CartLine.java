package com.example.zky.cart.model;

import com.example.zky.product.model.Product;
import lombok.*;

import javax.persistence.*;

/**
 * Created by Andreï on 30/06/2017 for ZKY.
 * Object cart line in db
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "CARTLINE")
public class CartLine {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "quantity")
    private int quantity;
//
    @ManyToOne
    @JoinColumn(name = "product")
    private Product product;
}
