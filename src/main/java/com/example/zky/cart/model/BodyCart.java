package com.example.zky.cart.model;

import lombok.*;

/**
 * Created by Kevin on 02/07/2017 for ZKY.
 * Object body cart with quantity and product id
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BodyCart {
    private int productId;
    private int quantity;
}
