package com.example.zky.account.model;

import lombok.*;

/**
 * Created by Kevin on 06/07/2017 for ZKY.
 * Body object passwords
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PasswordChange {
    String oldPassword;
    String newPassword;
}
