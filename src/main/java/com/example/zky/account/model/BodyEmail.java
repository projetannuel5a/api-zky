package com.example.zky.account.model;

import lombok.*;

/**
 * Created by Kevin on 10/07/2017 for ZKY.
 * Body object email
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BodyEmail {
    private String email;
}
