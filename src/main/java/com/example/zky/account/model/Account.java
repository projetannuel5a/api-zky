package com.example.zky.account.model;

/**
 * Created by Andreï on 02/04/2017 for ZKY.
 * Object account with column db
 */

import com.example.zky.cart.model.Cart;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ACCOUNT")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "isadmin")
    private boolean isAdmin;

    @OneToMany
    @JoinColumn(name = "oldcarts")
    private List<Cart> oldCarts;

    @OneToOne
    @JoinColumn(name="currentcart")
    private Cart currentCart;

    //columns from client csv
    @Column(name = "numdossier")
    private String numDossier;

    @Column(name = "nom")
    private String nom;

    @Column(name = "identifiant")
    private String identifiant;

    @Column(name = "civ")
    private String civ;

    @Column(name = "cp")
    private String cp;

    @Column(name = "ville")
    private String ville;

    @Column(name = "pays")
    private String pays;

    @Column(name = "enseigne")
    private String enseigne;

    @Column(name = "telephone1")
    private String telephone1;

    @Column(name = "telephone2")
    private String telephone2;

    @Column(name = "fax")
    private String fax;

    @Column(name = "portable1")
    private String portable1;

    @Column(name = "administration")
    private String administration;

    @Column(name = "representant2")
    private String representant2;

    @Column(name = "representant3")
    private String representant3;

    @Column(name = "tournee")
    private String tournee;

    @Column(name = "soustournee")
    private String sousTournee;

    @Column(name = "codegeographique")
    private String codeGeographique;

    @Column(name = "agence")
    private String agence;

    @Column(name = "regroupement")
    private String regroupement;

    @Column(name = "familleclient")
    private String familleClient;

    @Column(name = "sousfamilleclient")
    private String sousFamilleClient;

    @Column(name = "naf08")
    private String naf08;

    @Column(name = "compte")
    private String compte;

    @Column(name = "codeactivite")
    private String codeActivite;

    @Column(name = "email")
    private String email;

    @Column(name = "contact")
    private String contact;

    @Column(name = "siret")
    private String siret;

    @Column(name = "numvoie")
    private String numVoie;

    @Column(name = "btq")
    private String btq;

    @Column(name = "nomdelavoie")
    private String nomdelavoie;

    @Column(name = "complementadresse")
    private String complementAdresse;

    @Column(name = "naf")
    private String naf;
}
