package com.example.zky.account.model;

import lombok.*;

/**
 * Created by Kevin on 06/07/2017 for ZKY.
 * Body object change user info
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {

    private String numDossier;
    private String nom;
    private String cp;
    private String ville;
    private String pays;
    private String enseigne;
    private String telephone1;
    private String telephone2;
    private String fax;
    private String administration;
    private String compte;
    private String email;
    private String siret;
    private String numVoie;
    private String nomdelavoie;
    private String complementAdresse;
    private boolean isAdmin;
}
