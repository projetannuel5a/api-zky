package com.example.zky.account.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Kevin on 07/07/2017 for ZKY.
 * Body objet and column db for Zky information
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "ZKY")
public class ZkyInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "address")
    private String address;

    @Column(name = "tel")
    private String tel;

    @Column(name = "mail")
    private String mail;
}
