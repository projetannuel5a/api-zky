package com.example.zky.account.model;

import lombok.*;

/**
 * Created by Andreï on 17/04/2017 for ZKY.
 * Model for Data of Account
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountData {

    private AccountPublic accountPublic;

    
}
