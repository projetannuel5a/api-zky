package com.example.zky.account.repository;

import com.example.zky.account.model.ZkyInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Kevin on 07/07/2017 for ZKY.
 * Repository for zky info
 */
@Repository
public interface ZkyInfoRepository  extends JpaRepository<ZkyInfo, Integer> {
}
