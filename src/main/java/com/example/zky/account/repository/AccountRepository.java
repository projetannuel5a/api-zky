package com.example.zky.account.repository;

import com.example.zky.account.model.Account;
import com.example.zky.cart.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Andreï on 02/04/2017 for ZKY.
 * Repository for Account
 */

@Repository
public interface AccountRepository extends JpaRepository<Account, String>{

    List<Account> findAll();
    List<Account> findByLogin( String login );
    List<Account> findById( int id );
    Account findAccountByCurrentCart(Cart cart);
    Account findAccountByOldCartsIsContaining(Cart cart);
    Account findAccountByPassword(String password);
    Account findAccountByEmail(String email);
}
