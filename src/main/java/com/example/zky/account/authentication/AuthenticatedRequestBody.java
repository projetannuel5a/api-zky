package com.example.zky.account.authentication;

import lombok.*;

/**
 * Created by Andreï on 11/04/2017 for ZKY.
 * Default body for retrieving data + token
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticatedRequestBody<T> {
    private T body;
    private String token;
}
