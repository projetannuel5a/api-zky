package com.example.zky.account.controller;

import au.com.bytecode.opencsv.CSVReader;
import com.example.zky.account.authentication.AuthenticatedRequestBody;
import com.example.zky.account.authentication.AuthenticationManager;
import com.example.zky.account.authentication.ConnectionRequestBody;
import com.example.zky.account.authentication.Token;
import com.example.zky.account.exceptions.AccountNotFoundException;
import com.example.zky.account.exceptions.IncorrectCsvException;
import com.example.zky.account.exceptions.MustBeAuthenticatedAsAdminException;
import com.example.zky.account.exceptions.SamePasswordException;
import com.example.zky.account.model.*;
import com.example.zky.account.repository.ZkyInfoRepository;
import com.example.zky.account.service.AccountService;
import com.example.zky.config.PasswordGenerator;
import com.example.zky.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


/**
 * Created by Andreï on 02/04/2017 for ZKY.
 */

@RestController
@CrossOrigin
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;
    private final ZkyInfoRepository zkyInfoRepository;
    private final MailService emailService;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public AccountController(AccountService accountService, ZkyInfoRepository zkyInfoRepository, MailService emailService, AuthenticationManager authenticationManager) {
        this.accountService = accountService;
        this.zkyInfoRepository = zkyInfoRepository;
        this.emailService = emailService;
        this.authenticationManager = authenticationManager;
    }


    /**
     * Get account information.
     *
     * @param token token user
     * @return Account Data
     */
    @RequestMapping(value = "/my-account", method = POST)
    public AccountData getMyAccount(@RequestBody Token token) {
        Account account = authenticationManager.getAccountFromToken(token.getToken());
        AccountPublic accountP = new AccountPublic(account);
        return new AccountData(accountP);
    }


    /**
     * Authenticate Account using login and password
     *
     * @param connectionRequestBody login and password
     * @return token
     */
    @RequestMapping(value = "/authenticate", method = POST)
    public Token authenticateAccount(@RequestBody ConnectionRequestBody connectionRequestBody) {
        return new Token(authenticationManager.getTokenByAuthentication(connectionRequestBody.getLogin(), connectionRequestBody.getPassword()));
    }


    /**
     * Change user password
     *
     * @param password new password and old password
     * @return token
     * @throws Exception issue
     */
    @RequestMapping(value = "/password", method = POST)
    public Token changePassword(@RequestBody AuthenticatedRequestBody<PasswordChange> password) throws Exception {
        authenticationManager.mustBeValidToken(password.getToken());
        accountService.validatePassword(password.getBody().getNewPassword());

        Account account = authenticationManager.getAccountFromToken(password.getToken());
        if (!accountService.isSamePassword(password.getBody().getOldPassword(), account.getPassword())) {
            throw new SamePasswordException();
        }
        account.setPassword(password.getBody().getNewPassword());
        accountService.updateAccount(account);

        return new Token(authenticationManager.getTokenByAccount(account));
    }


    /**
     * Change user info.
     *
     * @param info all needed info.
     * @return AccountPublic
     */
    @RequestMapping(value = "/update", method = POST)
    public AccountPublic changeInfo(@RequestBody AuthenticatedRequestBody<UserInfo> info) {
        authenticationManager.mustBeValidToken(info.getToken());
        Account account = authenticationManager.getAccountFromToken(info.getToken());
        if (info.getBody().getAdministration() != null) {
            if (!info.getBody().getAdministration().isEmpty()) {
                if (!info.getBody().getAdministration().equals(account.getAdministration())) {
                    account.setAdministration(info.getBody().getAdministration());
                }
            }
        }
        if (info.getBody().getCp() != null) {
            if (!info.getBody().getCp().isEmpty()) {
                if (!info.getBody().getCp().equals(account.getCp())) {
                    account.setCp(info.getBody().getCp());
                }
            }
        }
        if (info.getBody().getVille() != null) {
            if (!info.getBody().getVille().isEmpty()) {
                if (!info.getBody().getVille().equals(account.getVille())) {
                    account.setVille(info.getBody().getVille());
                }
            }
        }
        if (info.getBody().getPays() != null) {
            if (!info.getBody().getPays().isEmpty()) {
                if (!info.getBody().getPays().equals(account.getPays())) {
                    account.setPays(info.getBody().getPays());
                }
            }
        }
        if (info.getBody().getEnseigne() != null) {
            if (!info.getBody().getEnseigne().isEmpty()) {
                if (!info.getBody().getEnseigne().equals(account.getEnseigne())) {
                    account.setEnseigne(info.getBody().getEnseigne());
                }
            }
        }
        if (info.getBody().getTelephone1() != null) {
            if (!info.getBody().getTelephone1().isEmpty()) {
                if (!info.getBody().getTelephone1().equals(account.getTelephone1())) {
                    account.setTelephone1(info.getBody().getTelephone1());
                }
            }
        }
        if (info.getBody().getTelephone2() != null) {
            if (!info.getBody().getTelephone2().isEmpty()) {
                if (!info.getBody().getTelephone2().equals(account.getTelephone2())) {
                    account.setTelephone2(info.getBody().getTelephone2());
                }
            }
        }
        if (info.getBody().getFax() != null) {
            if (!info.getBody().getFax().isEmpty()) {
                if (!info.getBody().getFax().equals(account.getFax())) {
                    account.setFax(info.getBody().getFax());
                }
            }
        }
        if (info.getBody().getEmail() != null) {
            if (!info.getBody().getEmail().isEmpty()) {
                if (!info.getBody().getEmail().equals(account.getEmail())) {
                    account.setEmail(info.getBody().getEmail());
                }
            }
        }
        if (info.getBody().getSiret() != null) {
            if (!info.getBody().getSiret().isEmpty()) {
                if (!info.getBody().getSiret().equals(account.getSiret())) {
                    account.setSiret(info.getBody().getSiret());
                }
            }
        }
        if (info.getBody().getNumVoie() != null) {
            if (!info.getBody().getNumVoie().isEmpty()) {
                if (!info.getBody().getNumVoie().equals(account.getNumVoie())) {
                    account.setNumVoie(info.getBody().getNumVoie());
                }
            }
        }
        if (info.getBody().getComplementAdresse() != null) {
            if (!info.getBody().getComplementAdresse().isEmpty()) {
                if (!info.getBody().getComplementAdresse().equals(account.getComplementAdresse())) {
                    account.setComplementAdresse(info.getBody().getComplementAdresse());
                }
            }
        }
        if (info.getBody().getNumDossier() != null) {
            if (!info.getBody().getNumDossier().isEmpty()) {
                if (!info.getBody().getNumDossier().equals(account.getNumDossier())) {
                    account.setNumDossier(info.getBody().getNumDossier());
                }
            }
        }
        if (info.getBody().getCompte() != null) {
            if (!info.getBody().getCompte().isEmpty()) {
                if (!info.getBody().getCompte().equals(account.getCompte())) {
                    account.setCompte(info.getBody().getCompte());
                }
            }
        }
        if (info.getBody().getNomdelavoie() != null) {
            if (!info.getBody().getNomdelavoie().isEmpty()) {
                if (!info.getBody().getNomdelavoie().equals(account.getNomdelavoie())) {
                    account.setNomdelavoie(info.getBody().getNomdelavoie());
                }
            }
        }
        if (info.getBody().getNom() != null) {
            if (!info.getBody().getNom().isEmpty()) {
                if (!info.getBody().getNom().equals(account.getNom())) {
                    account.setNom(info.getBody().getNom());
                }
            }
        }

        accountService.saveAccount(account);
        return new AccountPublic(account);
    }

    /**
     * Change Zky information need to be admin
     *
     * @param info zky info
     * @return ZkyInfo
     */
    @RequestMapping(value = "/info", method = POST)
    public ZkyInfo changeZkyInfo(@RequestBody AuthenticatedRequestBody<ZkyInfo> info) throws Exception {
        authenticationManager.mustBeValidToken(info.getToken());
        Account account = authenticationManager.getAccountFromToken(info.getToken());
        if (account.isAdmin()) {
            ZkyInfo zkyInfo = zkyInfoRepository.findAll().get(0);
            ZkyInfo newZkyInfo = info.getBody();
            if (zkyInfo != null) {
                newZkyInfo.setId(zkyInfo.getId());
            }
            zkyInfoRepository.save(newZkyInfo);
            return newZkyInfo;
        } else {
            throw new MustBeAuthenticatedAsAdminException();
        }
    }

    /**
     * Get all users info need to be admin
     *
     * @param token token
     * @return list account public
     */
    @RequestMapping(value = "/users")
    public List<AccountPublic> getAllUsersInfo(@RequestBody Token token) throws Exception {
        authenticationManager.mustBeValidToken(token.getToken());
        Account account = authenticationManager.getAccountFromToken(token.getToken());
        List<AccountPublic> accountPublics = new ArrayList<>();
        if (account.isAdmin()) {
            List<Account> accounts = accountService.getAccounts();
            for (Account a :
                    accounts) {
                accountPublics.add(new AccountPublic(a));
            }
            return accountPublics;
        } else {
            throw new MustBeAuthenticatedAsAdminException();
        }
    }

    /**
     * Get Zky info
     *
     * @return zky info
     */
    @RequestMapping(value = "/info", method = GET)
    public ZkyInfo getZkyInfo() {
        List<ZkyInfo> zkyInfo = zkyInfoRepository.findAll();
        ZkyInfo zkyInfo1;
        if (zkyInfo.isEmpty()) {
            zkyInfo1 = new ZkyInfo();
            zkyInfo1.setAddress("8 rue Lavoisier - 77330 Ozoir la Ferrière - France");
            zkyInfo1.setMail("zky@live.fr");
            zkyInfo1.setTel("+33 (0)1 60 02 89 47");
            zkyInfoRepository.save(zkyInfo1);
        } else {
            zkyInfo1 = zkyInfo.get(0);
        }
        return zkyInfo1;

    }

    /**
     * Create account with csv list , checking email , creating login and password , and sending email.
     *
     * @param file  csv file
     * @param token token admin
     * @return boolean
     * @throws Exception fail csv
     */
    @RequestMapping(value = "/upload", method = POST)
    @ResponseStatus(HttpStatus.CREATED)
    public boolean addAccountCsv(@RequestParam("file") MultipartFile file,
                                 @RequestParam String token) throws Exception {

        authenticationManager.mustBeValidToken(token);
        if (authenticationManager.getAccountFromToken(token).isAdmin()) {
            if (!file.isEmpty()) {
                CSVReader reader;

                reader = new CSVReader(
                        new InputStreamReader(file.getInputStream(), "windows-1252"),
                        ';', '"', 0);

                String[] record;
                List<Account> accounts = new ArrayList<>();


                boolean firstLine = true;
                HashMap<Integer, String> map = new HashMap<>();
                try {
                    while ((record = reader.readNext()) != null) {
                        if (firstLine) {
                            for (int i = 0; i < record.length; i++) {
                                map.put(i, record[i].trim());
                            }
                            firstLine = false;
                        } else {

                            Account account = new Account();
                            for (int i = 0; i < record.length; i++) {

                                switch (map.get(i)) {
                                    case "N° Dossier":
                                        account.setNumDossier(record[i]);
                                        break;
                                    case "Nom":
                                        account.setNom(record[i]);
                                        break;
                                    case "Identifiant":
                                        account.setIdentifiant(record[i]);
                                        break;
                                    case "Civ.":
                                        account.setCiv(record[i]);
                                        break;
                                    case "CP":
                                        account.setCp(record[i]);
                                        break;
                                    case "Ville":
                                        account.setVille(record[i]);
                                        break;
                                    case "Pays":
                                        account.setPays(record[i]);
                                        break;
                                    case "Enseigne":
                                        account.setEnseigne(record[i]);
                                        break;
                                    case "Téléphone 1":
                                        account.setTelephone1(record[i]);
                                        break;
                                    case "Téléphone 2":
                                        account.setTelephone2(record[i]);
                                        break;
                                    case "Fax":
                                        account.setFax(record[i]);
                                        break;
                                    case "Portable1":
                                        account.setPortable1(record[i]);
                                        break;
                                    case "Administration":
                                        account.setAdministration(record[i]);
                                        break;
                                    case "Représentant 2":
                                        account.setRepresentant3(record[i]);
                                        break;
                                    case "Représentant 3":
                                        account.setRepresentant3(record[i]);
                                        break;
                                    case "Tournée":
                                        account.setTournee(record[i]);
                                        break;
                                    case "Sous-Tournée":
                                        account.setSousTournee(record[i]);
                                        break;
                                    case "Code géographique":
                                        account.setCodeGeographique(record[i]);
                                        break;
                                    case "Agence":
                                        account.setAgence(record[i]);
                                        break;
                                    case "Regroupement":
                                        account.setRegroupement(record[i]);
                                        break;
                                    case "Famille client":
                                        account.setFamilleClient(record[i]);
                                        break;
                                    case "Sous-famille client":
                                        account.setSousFamilleClient(record[i]);
                                        break;
                                    case "NAF 08":
                                        account.setNaf08(record[i]);
                                        break;
                                    case "Compte":
                                        account.setCompte(record[i]);
                                        break;
                                    case "Code Activité":
                                        account.setCodeActivite(record[i]);
                                        break;
                                    case "E-Mail":
                                        account.setEmail(record[i]);
                                        break;
                                    case "Contact":
                                        account.setContact(record[i]);
                                        break;
                                    case "Siret":
                                        account.setSiret(record[i]);
                                        break;
                                    case "N°Voie":
                                        account.setNumVoie(record[i]);
                                        break;
                                    case "BTQ":
                                        account.setBtq(record[i]);
                                        break;
                                    case "Nom de la voie":
                                        account.setNomdelavoie(record[i]);
                                        break;
                                    case "Complément d'adresse":
                                        account.setComplementAdresse(record[i]);
                                        break;
                                    case "NAF":
                                        account.setNaf(record[i]);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if (!account.getEmail().isEmpty()) {
                                accountService.validateLogin(account.getEmail());
                                account.setLogin(account.getEmail());

                                PasswordGenerator passwordGenerator = new PasswordGenerator();
                                String password = passwordGenerator.nextSessionId();
                                account.setPassword(password);
                                accounts.add(account);

                                emailService.accountCreated(account.getLogin(), account.getLogin(), password);

                            }
                        }

                    }
                    accountService.saveAccounts(accounts);
                    return true;
                } catch (Exception e) {
                    throw new IncorrectCsvException();
                }
            } else {
                throw new IncorrectCsvException();
            }
        }
        throw new MustBeAuthenticatedAsAdminException();
    }


    /**
     * Send email checking if the person asked for new password
     *
     * @param email email user
     * @throws Exception Account not valid
     */
    @RequestMapping(value = "/restore", method = POST)
    public boolean askPassword(@RequestBody BodyEmail email) throws Exception {
        try {
            Account account = accountService.findByEmail(email.getEmail());
            if (account != null) {

//                emailService.restorePasswordConfirmation(email.getEmail(), "localhost:8080/account/restore/" + email.getEmail() + "/" + account.getPassword());
                emailService.restorePasswordConfirmation(email.getEmail(), "https://zky-catalogue.herokuapp.com/account/restore/" + email.getEmail() + "/" + account.getPassword());
                return true;
            } else {
                throw new AccountNotFoundException();
            }

        } catch (Exception e) {
            throw new AccountNotFoundException();
        }
    }

    /**
     * Send new password using url formed by email and password hashed
     *
     * @param email email user
     * @param id    password user
     * @return boolean
     * @throws Exception Account not valid
     */
    @RequestMapping(value = "/restore/{email}/{id}", method = GET)
    public boolean restoreEmail(@PathVariable(value = "email") String email, @PathVariable(value = "id") String id) throws Exception {
        try {
            Account account = accountService.findByEmail(email);
            if (account != null) {
                if (account.getPassword().equals(id)) {
                    PasswordGenerator passwordGenerator = new PasswordGenerator();
                    String password = passwordGenerator.nextSessionId();
                    emailService.restorePasswordSend(email, password);
                    account.setPassword(password);
                    accountService.updateAccount(account);
                }
            } else {
                throw new AccountNotFoundException();
            }

        } catch (Exception e) {
            throw new AccountNotFoundException();
        }
        return true;
    }


}
