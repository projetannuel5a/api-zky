package com.example.zky.product.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Andreï on 30/06/2017 for ZKY.
 * Object product in db
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "PRODUCT")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "img")
    private String img;

    @Column(name = "codearticle")
    private String codearticle;

    @Column(name = "ean13")
    private String ean13;

    @Column(name = "intitule1")
    private String intitule1;

    @Column(name = "prixdachatbrut")
    private String prixdachatbrut;

    @Column(name = "pamp")
    private String pamp;

    @Column(name = "prixdevente")
    private String prixdevente;

    @Column(name = "ttc")
    private String ttc;

    @Column(name = "unite")
    private String unite;

    @ManyToOne
    @JoinColumn(name = "fam")
    private Family fam;

    @ManyToOne
    @JoinColumn(name = "sfam")
    private SubFamily sfam;

    @Column(name = "fournisseur")
    private String fournisseur;

    @Column(name = "ventil")
    private String ventil;

    @Column(name = "tva")
    private String tva;

    @Column(name = "qtesurfacture")
    private String qtesurfacture;

    @Column(name = "libelleinterne")
    private String libelleinterne;

    @Column(name = "intitule2")
    private String intitule2;

    @Column(name = "intitule3")
    private String intitule3;

    @Column(name = "intitule4")
    private String intitule4;

    @Column(name = "nomenceurope")
    private String nomenceurope;

    @Column(name = "coderegroupt")
    private String coderegroupt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != product.id) return false;
        if (img != null ? !img.equals(product.img) : product.img != null) return false;
        if (codearticle != null ? !codearticle.equals(product.codearticle) : product.codearticle != null) return false;
        if (ean13 != null ? !ean13.equals(product.ean13) : product.ean13 != null) return false;
        if (intitule1 != null ? !intitule1.equals(product.intitule1) : product.intitule1 != null) return false;
        if (prixdachatbrut != null ? !prixdachatbrut.equals(product.prixdachatbrut) : product.prixdachatbrut != null)
            return false;
        if (pamp != null ? !pamp.equals(product.pamp) : product.pamp != null) return false;
        if (prixdevente != null ? !prixdevente.equals(product.prixdevente) : product.prixdevente != null) return false;
        if (ttc != null ? !ttc.equals(product.ttc) : product.ttc != null) return false;
        if (unite != null ? !unite.equals(product.unite) : product.unite != null) return false;
        if (fournisseur != null ? !fournisseur.equals(product.fournisseur) : product.fournisseur != null) return false;
        if (ventil != null ? !ventil.equals(product.ventil) : product.ventil != null) return false;
        if (tva != null ? !tva.equals(product.tva) : product.tva != null) return false;
        if (qtesurfacture != null ? !qtesurfacture.equals(product.qtesurfacture) : product.qtesurfacture != null)
            return false;
        if (libelleinterne != null ? !libelleinterne.equals(product.libelleinterne) : product.libelleinterne != null)
            return false;
        if (intitule2 != null ? !intitule2.equals(product.intitule2) : product.intitule2 != null) return false;
        if (intitule3 != null ? !intitule3.equals(product.intitule3) : product.intitule3 != null) return false;
        if (intitule4 != null ? !intitule4.equals(product.intitule4) : product.intitule4 != null) return false;
        if (nomenceurope != null ? !nomenceurope.equals(product.nomenceurope) : product.nomenceurope != null)
            return false;
        return coderegroupt != null ? coderegroupt.equals(product.coderegroupt) : product.coderegroupt == null;
    }

}
