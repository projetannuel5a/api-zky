package com.example.zky.product.model;

import lombok.*;

/**
 * Created by Kevin on 08/07/2017 for ZKY.
 * Version of the caller
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BodyVersion {
    Integer version;
}
