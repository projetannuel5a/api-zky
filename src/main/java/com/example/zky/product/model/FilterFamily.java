package com.example.zky.product.model;

import lombok.*;

import java.util.List;

/**
 * Created by Kevin on 03/07/2017 for ZKY.
 * Filter family for search
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilterFamily {
    List<Integer> family;
    List<Integer> subfamily;
    String search;

}
