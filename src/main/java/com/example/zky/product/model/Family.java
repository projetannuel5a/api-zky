package com.example.zky.product.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by moi on 30/06/2017 for ZKY.
 * Object family in db
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "CATEGORY")
public class Family {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "family")
    private int family;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family1 = (Family) o;

        if (family != family1.family) return false;
        return name != null ? name.equals(family1.name) : family1.name == null;
    }


    //    @Column(name = "subfamily")
//    private List<SubFamily> subCategories = new ArrayList<>();
}
