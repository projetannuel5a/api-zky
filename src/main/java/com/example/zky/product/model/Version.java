package com.example.zky.product.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Kevin on 07/07/2017 for ZKY.
 * Object version in db
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "VERSION")
public class Version {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "version")
    private int version;

    @ManyToMany
    @JoinColumn (name = "productList")
    private List<Product> productList;

    @ManyToMany
    @JoinColumn (name = "familyList")
    private List<Family> familyList;

    @ManyToMany
    @JoinColumn (name = "subFamilyList")
    private List<SubFamily> subFamilyList;


    public Version(int version, List<Product> productList, List<Family> familyList, List<SubFamily> subFamilyList) {
        this.version = version;
        this.productList = productList;
        this.familyList = familyList;
        this.subFamilyList = subFamilyList;
    }
}
