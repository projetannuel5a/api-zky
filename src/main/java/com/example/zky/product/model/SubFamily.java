package com.example.zky.product.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Kevin on 01/07/2017 for ZKY.
 * Object subfamily in db
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "SUBCATEGORY")
public class SubFamily {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "subfamily", nullable = false)
    private int subfamily;

    @ManyToOne
    @JoinColumn(name = "category")
    private Family family;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubFamily subFamily = (SubFamily) o;

        return subfamily == subFamily.subfamily && name.equals(subFamily.name);
    }


}
