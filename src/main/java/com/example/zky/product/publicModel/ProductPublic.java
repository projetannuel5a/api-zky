package com.example.zky.product.publicModel;

import com.example.zky.product.model.Product;
import lombok.*;

/**
 * Created by Kevin on 02/07/2017 for ZKY.
 * Object product public for call
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductPublic {
    private int id;
    private String codearticle;
    private String intitule1;
    private String prixdevente;
    private String libelleinterne;
    private String img;

    public ProductPublic(Product product) {
        this.id = product.getId();
        this.codearticle = product.getCodearticle();
        this.intitule1 = product.getIntitule1();
        this.prixdevente = product.getPrixdevente();
        this.libelleinterne = product.getLibelleinterne();
        this.img = product.getImg();
    }
}
