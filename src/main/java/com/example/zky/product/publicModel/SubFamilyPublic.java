package com.example.zky.product.publicModel;

import com.example.zky.product.model.SubFamily;
import lombok.*;

import java.util.List;

/**
 * Created by Kevin on 02/07/2017 for ZKY.
 * Object Subfimily for call
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubFamilyPublic {
    SubFamily subfamily;
    List<ProductPublic> products;
}
