package com.example.zky.product.publicModel;

import lombok.*;

import java.util.List;

/**
 * Created by Kevin on 04/07/2017 for ZKY.
 * Object product list for call
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ListProductPublic {
    private List<ProductPublic> productPublicList;
    private int pageCount;
}
