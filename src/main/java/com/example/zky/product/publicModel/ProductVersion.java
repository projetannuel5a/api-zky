package com.example.zky.product.publicModel;

import com.example.zky.product.model.Product;
import lombok.*;

/**
 * Created by Kevin on 11/07/2017 for ZKY.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductVersion {
    private int id;
    private String codearticle;
    private String intitule1;
    private String prixdevente;
    private String libelleinterne;
    private String img;
    private String familyName;
    private String subFamilyName;

    public ProductVersion(Product product) {
        this.id = product.getId();
        this.codearticle = product.getCodearticle();
        this.intitule1 = product.getIntitule1();
        this.prixdevente = product.getPrixdevente();
        this.libelleinterne = product.getLibelleinterne();
        this.img = product.getImg();
        this.familyName = product.getFam().getName();
        this.subFamilyName = product.getSfam().getName();
    }
}
