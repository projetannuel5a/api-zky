package com.example.zky.product.publicModel;

import com.example.zky.product.model.Family;
import lombok.*;

import java.util.List;

/**
 * Created by Kevin on 02/07/2017 for ZKY.
 * Object family public for call
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FamilyPublic {
    Family family;

    List<SubFamilyPublic> subfamilyPublics;
}
