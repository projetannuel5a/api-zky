package com.example.zky.product.publicModel;

import com.example.zky.product.model.Family;
import com.example.zky.product.model.SubFamily;
import lombok.*;

import java.util.List;

/**
 * Created by Kevin on 11/07/2017 for ZKY.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class VersionPublic {
    private int version;
    private List<ProductVersion> productList;
    private List<Family> familyList;
    private List<SubFamily> subFamilyList;

}
