package com.example.zky.product.controller;

import au.com.bytecode.opencsv.CSVReader;
import com.example.zky.account.authentication.AuthenticationManager;
import com.example.zky.account.exceptions.IncorrectCsvException;
import com.example.zky.account.exceptions.MustBeAuthenticatedAsAdminException;
import com.example.zky.product.model.*;
import com.example.zky.product.publicModel.*;
import com.example.zky.product.repository.FamilyRepository;
import com.example.zky.product.repository.ProductRepository;
import com.example.zky.product.repository.SubFamiliesRepository;
import com.example.zky.product.repository.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Kevin on 30/06/2017 for ZKY.
 */

@RestController
@CrossOrigin
@RequestMapping("/product")
public class ProductController {

    private final AuthenticationManager authenticationManager;
    private final ProductRepository productRepository;
    private final SubFamiliesRepository subFamiliesRepository;
    private final FamilyRepository familyRepository;
    private final VersionRepository versionRepository;

    @Autowired
    public ProductController(AuthenticationManager authenticationManager, ProductRepository productRepository, SubFamiliesRepository subFamiliesRepository, FamilyRepository familyRepository, VersionRepository versionRepository) {
        this.authenticationManager = authenticationManager;
        this.productRepository = productRepository;
        this.subFamiliesRepository = subFamiliesRepository;
        this.familyRepository = familyRepository;
        this.versionRepository = versionRepository;
    }

    /**
     * Get product by id
     *
     * @param id id product
     * @return Product
     */
    @RequestMapping(value = "/{id}", method = GET)
    public Product getProducts(@PathVariable("id") int id) {
        return productRepository.findById(id);
    }

    /**
     * Get filtered paginated product
     *
     * @param pageable Page and size requested
     * @param filter   Filter by family , subfamily and product
     * @return List product public
     */
    @RequestMapping(value = "/filter", method = POST)
    public ListProductPublic getProducts(@PageableDefault() Pageable pageable, @RequestBody FilterFamily filter) {
        List<Product> productList = new ArrayList<>();

        // Check if family or subfamily is checked
        if (filter.getSubfamily().isEmpty()) {
            for (Integer fam :
                    filter.getFamily()) {
                productList.addAll(productRepository.findProductsByFam(familyRepository.findByFamily(fam)));
            }
        } else {
            if (!filter.getFamily().isEmpty()) {
                for (Integer sfam : filter.getSubfamily()) {
                    productList.addAll(productRepository.findProductsBySfam(subFamiliesRepository.findBySubfamily(sfam)));
                }
            }
        }

        // check is product list is then , otherwise set all product in it
        if (productList.isEmpty()) {
            productList = (List<Product>) productRepository.findAll();
        }

        List<ProductPublic> productPublics = new ArrayList<>();
        //find by search
        for (Product p :
                productList) {
            if (filter.getSearch().isEmpty()) {
                productPublics.add(new ProductPublic(p));
            } else if (p.getCodearticle().toLowerCase().contains(filter.getSearch().toLowerCase()) ||
                    p.getLibelleinterne().toLowerCase().contains(filter.getSearch().toLowerCase()) ||
                    p.getIntitule1().toLowerCase().contains(filter.getSearch().toLowerCase())) {
                productPublics.add(new ProductPublic(p));
            }
        }

        //Pagination
        int toIndex = pageable.getPageNumber() + 1;
        if (toIndex < 0) {
            toIndex = pageable.getPageSize();
        } else {
            toIndex *= pageable.getPageSize();
        }
        if (toIndex > productPublics.size()) {
            toIndex = productPublics.size();
        }

        int index = pageable.getPageNumber();
        if (index < 0) {
            index = 0;
        } else if (index > 0) {
            index *= pageable.getPageSize();
        }

        int pageNumber;
        if ((productPublics.size() / pageable.getPageSize()) == 1) {
            pageNumber = 1;
        } else {
            pageNumber = Math.round(productPublics.size() / pageable.getPageSize()) + 1;
        }
        if (index > toIndex) {
            List<ProductPublic> fail = new ArrayList<>();
            return new ListProductPublic(fail, pageNumber);
        }

        productPublics = productPublics.subList(index, toIndex);

        return new ListProductPublic(productPublics, pageNumber);
    }

    /**
     * Get family containing subfamily containing products
     *
     * @return list Family public
     */
    @RequestMapping(value = "/family", method = GET)
    public List<FamilyPublic> getFamily() {
        List<FamilyPublic> familyPublics = new ArrayList<>();
        List<SubFamilyPublic> subFamilyPublics;
        List<ProductPublic> productPublics;
        List<Family> families = familyRepository.findAll();
        for (Family f :
                families) {
            subFamilyPublics = new ArrayList<>();
            List<SubFamily> subFamilies = subFamiliesRepository.findSubFamiliesByFamilyOrderBySubfamily(f);
            for (SubFamily s :
                    subFamilies) {
                productPublics = new ArrayList<>();
                List<Product> products = productRepository.findProductsBySfam(s);
                for (Product p :
                        products) {
                    productPublics.add(new ProductPublic(p));
                }
                subFamilyPublics.add(new SubFamilyPublic(s, productPublics));
            }
            familyPublics.add(new FamilyPublic(f, subFamilyPublics));
        }
        return familyPublics;
    }


    /**
     * Upload csv of product
     *
     * @param file  csv of Zky products
     * @param token token admin
     * @return boolean
     * @throws Exception not admin / file empty
     */
    @RequestMapping(value = "/upload", method = POST)
    public boolean addProductCsv(@RequestParam("file") MultipartFile file,
                                 @RequestParam String token) throws Exception {

        authenticationManager.mustBeValidToken(token);
        if (authenticationManager.getAccountFromToken(token).isAdmin()) {
            if (!file.isEmpty()) {
                return parseCsv(file);
            } else {
                throw new IncorrectCsvException();
            }
        } else {
            throw new MustBeAuthenticatedAsAdminException();
        }
    }

    /**
     * Get version of data
     *
     * @param version version caller
     * @return Version of products
     */
    @RequestMapping(value = "/version", method = POST)
    public VersionPublic getVersion(@RequestBody BodyVersion version) {
        List<Version> versionList = versionRepository.findAll();
        VersionPublic versionPublic = new VersionPublic();
        List<Family> families = new ArrayList<>();
        List<SubFamily> subFamilies = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        List<ProductVersion> productPublics = new ArrayList<>();

        int apiVersion = versionList.size();
        if (version.getVersion() < apiVersion) {
            for (int i = version.getVersion(); i < apiVersion; i++) {
                families.addAll(versionList.get(i).getFamilyList());
                subFamilies.addAll(versionList.get(i).getSubFamilyList());
                products.addAll(versionList.get(i).getProductList());
            }
            for (Product p :
                    products) {
                productPublics.add(new ProductVersion(p));
            }
            versionPublic.setFamilyList(families);
            versionPublic.setSubFamilyList(subFamilies);
            versionPublic.setProductList(productPublics);
        }
        versionPublic.setVersion(apiVersion);


        return versionPublic;
    }

    /**
     * Update version of api data
     *
     * @param products    list of product
     * @param subFamilies list of subFamilies
     * @param families    list of families
     */
    private void updateVersion(List<Product> products, List<SubFamily> subFamilies, List<Family> families) {
        List<Version> version = versionRepository.findAll();


        if (version.isEmpty()) {
            versionRepository.save(new Version(1, products, families, subFamilies));
        } else {
            versionRepository.save(new Version(version.size() + 1, products, families, subFamilies));
        }

    }


    /**
     * Parse CSV , set in db and update version
     *
     * @param file csv file
     * @return boolean
     * @throws Exception CSV non valide
     */
    private boolean parseCsv(MultipartFile file) throws Exception {
        CSVReader reader;
        try {
            //Retrieve Names
            HashMap<String, HashMap<String, String>> familyNames = new HashMap<>();
            reader = new CSVReader(
                    new InputStreamReader(new FileInputStream("src/main/resources/static/data/csv/subfamily.csv"), "windows-1252"),
                    ';', '"', 1);
            String[] recordNames;
            while ((recordNames = reader.readNext()) != null) {
                HashMap<String, String> subfamilyNames = new HashMap<>();
                if (!familyNames.containsKey(recordNames[0])) {
                    subfamilyNames.put(recordNames[1], recordNames[2]);
                    familyNames.put(recordNames[0], subfamilyNames);
                } else {
                    subfamilyNames = familyNames.get(recordNames[0]);
                    subfamilyNames.put(recordNames[1], recordNames[2]);
                    familyNames.put(recordNames[0], subfamilyNames);
                }
            }
            //read csv content
            reader = new CSVReader(
                    new InputStreamReader(file.getInputStream(), "windows-1252"),
                    ';', '"', 0);
            List<Product> products = new ArrayList<>();
            String[] record;
            HashMap<Integer, Family> categoryFamily = new HashMap<>();
            HashMap<String, SubFamily> subCategoryFamily = new HashMap<>();


            boolean firstLine = true;
            HashMap<Integer, String> mapHeader = new HashMap<>();

            while ((record = reader.readNext()) != null) {

                //setup mapHeader
                if (firstLine) {
                    for (int i = 0; i < record.length; i++) {
                        mapHeader.put(i, record[i].trim());
                    }
                    firstLine = false;
                } else {
                    if (!record[8].isEmpty()) {
                        Family family;
                        SubFamily subFamily;
                        if (categoryFamily.containsKey(Integer.valueOf(record[8]))) {
                            family = categoryFamily.get(Integer.valueOf(record[8]));
                        } else {
                            family = new Family();
                            family.setFamily(Integer.valueOf(record[8]));
                            family.setName(familyNames.get(record[8]).get("*"));
                        }

                        if (subCategoryFamily.containsKey(record[8] + record[9])) {
                            subFamily = subCategoryFamily.get(record[8] + record[9]);
                        } else {
                            subFamily = new SubFamily();
                            int sfam = Integer.valueOf(record[9]);
                            subFamily.setSubfamily(Integer.valueOf(record[8] + sfam));
                            subFamily.setName(familyNames.get(record[8]).get(record[9]));
                        }

                        Product product = new Product();
                        for (int i = 0; i < record.length; i++) {

                            switch (mapHeader.get(i)) {
                                case "Code article":
                                    product.setCodearticle(record[i]);
                                    product.setImg("http://zky-catalogue.herokuapp.com/data/img/" + record[i] + ".jpg");
//                                    product.setImg("localhost:8080/data/img/" + record[i] + ".jpg");

                                    break;
                                case "EAN 13":
                                    product.setEan13(record[i]);
                                    break;
                                case "Intitulé 1":
                                    product.setIntitule1(record[i]);
                                    break;
                                case "Prix d'Achat Brut":
                                    product.setPrixdachatbrut(record[i].replace(",", "."));
                                    break;
                                case "PAMP":
                                    product.setPamp(record[i].replace(",", "."));
                                    break;
                                case "Prix de vente":
                                    product.setPrixdevente(record[i].replace(",", "."));
                                    break;
                                case "TTC":
                                    product.setTtc(record[i]);
                                    break;
                                case "Unité":
                                    product.setUnite(record[i]);
                                    break;
                                case "Fournisseur":
                                    product.setFournisseur(record[i]);
                                    break;
                                case "Ventil":
                                    product.setVentil(record[i]);
                                    break;
                                case "TVA":
                                    product.setTva(record[i]);
                                    break;
                                case "Qté sur facture":
                                    product.setQtesurfacture(record[i]);
                                    break;
                                case "Libellé interne":
                                    product.setLibelleinterne(record[i]);
                                    break;
                                case "Intitulé 2":
                                    product.setIntitule2(record[i]);
                                    break;
                                case "Intitulé 3":
                                    product.setIntitule3(record[i]);
                                    break;
                                case "Intitulé 4":
                                    product.setIntitule4(record[i]);
                                    break;
                                case "Nomenc. Europe":
                                    product.setNomenceurope(record[i]);
                                    break;
                                case "Code Regroupt.":
                                    product.setCoderegroupt(record[i]);
                                    break;
                                default:
                                    break;
                            }

                        }
                        product.setFam(family);
                        product.setSfam(subFamily);
                        products.add(product);
                        subFamily.setFamily(family);

                        categoryFamily.put(Integer.valueOf(record[8]), family);
                        subCategoryFamily.put(record[8] + record[9], subFamily);
                    }

                }
            }
            //Version
            List<Family> versionFamilies = new ArrayList<>();
            List<SubFamily> versionSubFamilies = new ArrayList<>();
            List<Product> versionProduct = new ArrayList<>();

            //Check values
            List<Family> oldFamilies = familyRepository.findAll();
            List<SubFamily> oldSubFamilies = subFamiliesRepository.findAll();
            List<Product> oldProduct = (List<Product>) productRepository.findAll();

            if (!oldFamilies.isEmpty() && !oldSubFamilies.isEmpty() && !oldProduct.isEmpty()) {
                //check produit
                for (Product oldP :
                        oldProduct) {
                    for (Product newP :
                            products) {
                        if (newP.getCodearticle().equals(oldP.getCodearticle())) {
                            newP.setId(oldP.getId());
                            if (!versionProduct.contains(newP)) {
                                if (!newP.equals(oldP)) {
                                    versionProduct.add(newP);
                                }
                            }
                        }
                    }
                }
                if (products.size() > oldProduct.size()) {
                    for (int i = oldProduct.size() ; i < products.size(); i++) {
                        versionProduct.add(products.get(i));
                    }
                }
            }

            List<Family> families = new ArrayList<>();
            List<SubFamily> subFamilies = new ArrayList<>();

            Set<Integer> keys = categoryFamily.keySet();
            for (Integer key :
                    keys) {
                Family family = categoryFamily.get(key);
                for (Family f :
                        oldFamilies) {
                    if (f.getName().equals(family.getName())) {
                        family.setId(f.getId());
                        if (!versionFamilies.equals(family)) {
                            if (!family.equals(f)) {
                                versionFamilies.add(family);
                            }
                        }
                    }
                }
                families.add(family);
                familyRepository.save(family);
            }

            Set<String> keysString = subCategoryFamily.keySet();
            for (String key :
                    keysString) {
                SubFamily subfamily = subCategoryFamily.get(key);
                for (SubFamily sf :
                        oldSubFamilies) {
                    if (sf.getName().equals(subfamily.getName())) {
                        subfamily.setId(sf.getId());
                        if (!versionSubFamilies.equals(subfamily)) {
                            if (!subfamily.equals(sf)) {
                                versionSubFamilies.add(subfamily);
                            }
                        }
                    }
                }
                subFamilies.add(subfamily);
                subFamiliesRepository.save(subfamily);
            }
            productRepository.save(products);

            if (oldFamilies.isEmpty() && oldSubFamilies.isEmpty() && oldProduct.isEmpty()) {
                versionFamilies.addAll(families);
                versionProduct.addAll(products);
                versionSubFamilies.addAll(subFamilies);
            }

            if (!versionFamilies.isEmpty() || !versionProduct.isEmpty() || !versionSubFamilies.isEmpty()) {
                updateVersion(versionProduct, versionSubFamilies, versionFamilies);
            }
            return true;

        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
            throw new IncorrectCsvException();
        }
    }
}
