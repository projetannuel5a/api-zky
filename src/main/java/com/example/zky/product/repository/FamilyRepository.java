package com.example.zky.product.repository;

import com.example.zky.product.model.Family;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Kevin on 01/07/2017 for ZKY.
 * Repository for family
 */
@Repository
public interface FamilyRepository extends JpaRepository<Family, Integer> {
    Family findByFamily(Integer id);
}
