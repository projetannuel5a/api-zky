package com.example.zky.product.repository;

import com.example.zky.product.model.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Kevin on 07/07/2017 for ZKY.
 * Repository for version
 */

//TODO remove?
@Repository
public interface VersionRepository extends JpaRepository<Version, Integer> {
    Version findByVersion(int version);
}
