package com.example.zky.product.repository;

import com.example.zky.product.model.Family;
import com.example.zky.product.model.SubFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Kevin on 01/07/2017 for ZKY.
 * Repository for sub family
 */
@Repository
public interface SubFamiliesRepository extends JpaRepository<SubFamily, Integer> {
    List<SubFamily> findSubFamiliesByFamilyOrderBySubfamily(Family family);
    List<SubFamily> findAllByOrderBySubfamily();
    SubFamily findBySubfamily(Integer id);
}
