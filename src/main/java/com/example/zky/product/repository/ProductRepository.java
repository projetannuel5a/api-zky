package com.example.zky.product.repository;

import com.example.zky.product.model.Family;
import com.example.zky.product.model.Product;
import com.example.zky.product.model.SubFamily;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Kevin on 30/06/2017 for ZKY.
 * Repository for product
 */
@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {

    List<Product> findProductsBySfam(SubFamily subFamily);
    List<Product> findProductsByFam(Family family);

    Product findByCodearticle(String codearticle);

    Product findById(int id);
}
