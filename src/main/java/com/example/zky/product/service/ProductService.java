package com.example.zky.product.service;

import com.example.zky.product.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Kevin on 30/06/2017 for ZKY.
 * Interface to define pageable
 */
public interface  ProductService {
    Page<Product> listAllByPage(Pageable pageable);
}
