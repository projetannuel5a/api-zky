package com.example.zky.product.service;

import com.example.zky.product.model.Product;
import com.example.zky.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Kevin on 30/06/2017 for ZKY.
 * implementation of Pageable
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Page<Product> listAllByPage(Pageable pageable) {
        return productRepository.findAll(pageable);
    }
}
