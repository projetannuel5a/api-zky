package com.example.zky.mail;

import com.example.zky.cart.model.CartLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Kevin on 09/07/2017 for ZKY.
 */
@Service
public class MailService {

    private JavaMailSender mailSender;
    private MailContentBuilder mailContentBuilder;

    @Autowired
    public MailService(JavaMailSender mailSender, MailContentBuilder mailContentBuilder) {
        this.mailSender = mailSender;
        this.mailContentBuilder = mailContentBuilder;
    }

    public void restorePasswordConfirmation(String to, String text) {
        try {

            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(to);
                messageHelper.setSubject("ZKY - Reinitialisation de mot de passe");
                String content = mailContentBuilder.buildRestorePasswordConfirmation(text);
                messageHelper.setText(content, true);
            };

            mailSender.send(messagePreparator);
        } catch (MailException e) {
            e.printStackTrace();
            // runtime exception; compiler will not force you to handle it
        }
    }

    public void restorePasswordSend(String to, String text) {
        try {

            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(to);
                messageHelper.setSubject("ZKY - Nouveau mot de passe");
                String content = mailContentBuilder.buildRestorePasswordSend(text);
                messageHelper.setText(content, true);
            };

            mailSender.send(messagePreparator);
        } catch (MailException e) {
            e.printStackTrace();
            // runtime exception; compiler will not force you to handle it
        }
    }
    public void accountCreated(String to, String login, String password) {
        try {

            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(to);
                messageHelper.setSubject("ZKY - Vos identifiants");
                String content = mailContentBuilder.buildAccountCreated(login, password);
                messageHelper.setText(content, true);
            };

            mailSender.send(messagePreparator);
        } catch (MailException e) {
            e.printStackTrace();
            // runtime exception; compiler will not force you to handle it
        }
    }

    public void validateCart(String to, String account, List<CartLine> cartLineList, String price, boolean isAdmin) {
        try {

            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(to);
                messageHelper.setSubject("ZKY - Récapitulatif commande");
                String content;
                if (isAdmin){
                    content = mailContentBuilder.buildValidateCartZky(account, cartLineList, price);
                }else{
                    content = mailContentBuilder.buildValidateCartConfirm(account, cartLineList, price);
                }
                messageHelper.setText(content, true);
            };

            mailSender.send(messagePreparator);
        } catch (MailException e) {
            e.printStackTrace();
            // runtime exception; compiler will not force you to handle it
        }
    }
}