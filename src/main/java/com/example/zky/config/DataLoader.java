package com.example.zky.config;

import com.example.zky.account.model.Account;
import com.example.zky.account.service.AccountService;
import com.example.zky.cart.model.Cart;
import com.example.zky.cart.model.Status;
import com.example.zky.cart.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by Kevin on 09/07/2017 for ZKY for ZKY.
 * Create at least one account when the server start
 */
@Component
public class DataLoader implements ApplicationRunner {

    private final AccountService accountService;
    private final CartRepository cartRepository;


    @Autowired
    public DataLoader(AccountService accountService, CartRepository cartRepository) {
        this.accountService = accountService;
        this.cartRepository = cartRepository;
    }

    /**
     * insert admin account if not existed
     * @param args args
     */
    public void run(ApplicationArguments args) {
        if (accountService.getAccounts().isEmpty()) {
            Cart cart = new Cart();
            cart.setStatus(Status.DOING);
            Account account = Account.builder().id(0).login("admin").email("andrei210@hotmail.fr").password("cXNH44GxgG").isAdmin(true).currentCart(cart).oldCarts(new ArrayList<>()).build();
            cartRepository.save(cart);
            accountService.updateAccount(account);
        }
    }
}