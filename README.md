# README #

### Compte: ###
* Module d'upload de csv de clients
* Module gestion des tokens
* Module récupération des données de son compte
* Module authentication
* Module modification de mot de passe
* Module modification des infos personnelles
* Module modification des infos ZKY
* Module récupération des infos ZKY

### Produit : ###
* Module d'upload de csv de produit
* Module gestion des versions
* Module récupération des produits par filtre et pagination
* Module récupération des produits avec famille et sous famille

### Panier : ###
* Module de récupération d’un panier par identifiant
* Module validation du panier avec identifiant
* Module récupération du panier courant
* Module récupération des anciens paniers
* Module d’ajout / modifier des quantités d’un produit dans le panier
* Module de validation de son panier
* Module récupération des statuts des paniers

### Mail : ###
* Envoi de mail pour récupération du mot de passe en 2 étape
* Envoi de mail automatique à l’importation d’un csv
* Envoi de mail confirmation de la commande